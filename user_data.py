import random
import string

class User_Annotate:
    def __init__(self):
        self.id = self.randomString()
        self.video_n_anno = []

    
    def randomString(self, stringLength=15):
        password_characters = string.ascii_letters + string.digits + string.punctuation
        return ''.join(random.choice(password_characters) for i in range(stringLength))
    
    def addAnnoVideo(self, vna):
        self.video_n_anno.append(vna)