import os
from flask import flash, Flask, render_template, redirect, url_for, request, json, session
from app import app
from werkzeug.utils import secure_filename
from user_data import User_Annotate
from datetime import datetime

# Currently these are the videos that we have available to annotate. You can add videos to this list and they will
# be added on the website. Or you can change them and they will be changed on the website. Please stick to the format of how they currently are now
# otherwise the code will not be able to process it correctly. The 3 keys are video, attribute-values, and sample_annotation
videos = [
    {"video" : "https://www.youtube.com/embed/BQXtdrQuHJ8", "att_val" : ["battery state : low battery", "object : stationary, straight ahead"], "sample_anno" : "You are on a collision course and have low battery take action immediately!"},
    {"video" : "https://www.youtube.com/embed/bONqVV34Dzc", "att_val" : ["battery state : low battery", "object : stationary, below", "frame condition : rear left propellor damaged", "weather : 80 kph wind", "out_of_range : almost out of range"], "sample_anno" : "You are dangerously low on battery. You are too far away and your rear left propellor is damaged. With strong winds a highspeed crash into the ground is imminant."}
]

# a list of all the users annotations
user_annotations = []



# HOME PAGE
@app.route('/home', methods=["GET"])
@app.route('/', methods=["GET"])
def index():
    if request.method == "POST":
        return redirect(url_for("home"))
    return render_template("home.html")


@app.route('/instructions', methods=["GET"])
def instructions():
    return render_template("instructionpage.html")


@app.route('/begin', methods=["GET"])
def begin():
    # Create new user object and to the user list, and set session values for the current user
    new_usr = User_Annotate()
    user_annotations.append(new_usr)
    session['current_usr'] = new_usr.id
    session['video_index'] = 0
    return redirect(url_for("annotate"))


@app.route('/annotate', methods=["GET", "POST"])
def annotate():
    if request.method == "POST":
        vid_ind = session.get('video_index', None) # get the current video index
        usr_curr = session.get('current_usr', None) # get the current user
        # find the current user object in our list
        for usr in user_annotations:
            if usr.id == usr_curr:
                anno = request.form.get("annotate", "") # get the user's annotation for the video
                #create a dictionary object to save the video link and annotation
                anvi = {
                    'annotation' : anno,
                    # 'video' : (videos[vid_ind])["video"],
                    'video' : videos[vid_ind],
                    'time' : datetime.now()
                }
                usr.addAnnoVideo(anvi) # add it to the user's list of annotations
                vid_ind = vid_ind + 1
                # checks to see if there are still videos left to annotate
                if (vid_ind > len(videos) - 1):
                    session['video_index'] = vid_ind
                    return redirect(url_for("thankyou"))
                elif (vid_ind <= len(videos)):
                    session['video_index'] = vid_ind
                    return redirect(url_for("annotate"))
                else:
                    return render_template("404.html")

    video_ind = session.get('video_index', None)
    
    url = videos[video_ind]
    return render_template("annotate.html", video = url)

# a simple thank you page
@app.route('/thankyou', methods=["GET"])
def thankyou():
    return render_template("thankyou.html")

# retrieves the annotated data in a table format 
@app.route('/retrieve_data', methods=["GET"])
def retrieve():
    # Gets all the data from the user's annotations
    list_annos = []
    for usr in user_annotations:
        # loop through each annotation andd append to the list
        for vna in usr.video_n_anno:
            list_annos.append(vna)
    # reverse the list so the handover messages go from most recent to last
    list_annos.reverse()
    print(list_annos)
    return render_template("retrieve_data.html", user_annotations = list_annos)

# returns the data as a json object
@app.route('/retrieve_data_json', methods=["GET"])
def retrieve_json():
    list_annos = []
    for usr in user_annotations:
        for anno in usr.video_n_anno:
            list_annos.append(anno)
    return json.dumps(list_annos)

# clears the data in the table
@app.route('/clear_data', methods=["POST", "GET"])
def clear_data():
    user_annotations.clear()
    print(user_annotations)
    return redirect(url_for("retrieve"))
