from flask import Flask

app = Flask(__name__)

app.secret_key = 'jeriah' #TODO: change to a much longer/stronger secret key
app.config['SECURITY_PASSWORD_SALT'] = 'caplinger' #TODO: change to a much longer/stronger secret key


app.config['SESSION_TYPE'] = 'filesystem'


from app import routes
