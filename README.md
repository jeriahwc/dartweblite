To run on a local machine, create a virtual environment and pip install the requirements from the file requirements.txt. 
Next, change directory until you are in the same directory as the file "application.py".
Run the python file by calling "python application.py" or "python3 application.py". Note, this application only works with python 3 and not python 2.
Now you should be able to follow the link to the local server.